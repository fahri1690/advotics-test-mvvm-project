package com.fahri.advotics.models

data class UserAccount(
    var name: String = "",
    var sex: String = "",
    var photo: String = "",
    var phone: String = ""
)