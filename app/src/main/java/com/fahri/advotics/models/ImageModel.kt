package com.fahri.advotics.models

data class ImageModel(
    var photo: String = ""
)