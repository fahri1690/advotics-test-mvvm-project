package com.fahri.advotics.`object`

import com.fahri.advotics.models.ImageModel

object ListImages {
    private val data = arrayOf(
        arrayOf(
           "https://instagram.fdps5-1.fna.fbcdn.net/v/t51.2885-15/e35/26158720_1812420125728909_4764439072930267136_n.jpg?_nc_ht=instagram.fdps5-1.fna.fbcdn.net&_nc_cat=101&oh=12ef5a9cca75700c270b6bb55b236c96&oe=5E8E905E"
        ),
        arrayOf(
            "https://instagram.fjog3-1.fna.fbcdn.net/v/t51.2885-15/e35/22708976_115118635918774_2757735356152414208_n.jpg?_nc_ht=instagram.fjog3-1.fna.fbcdn.net&_nc_cat=110&oh=7300a10d2f160261379a6eaad3d83803&oe=5E815A7A"
        ),
        arrayOf(
            "https://instagram.fdps5-1.fna.fbcdn.net/v/t51.2885-15/e35/20482250_1933406396937913_563221511318536192_n.jpg?_nc_ht=instagram.fdps5-1.fna.fbcdn.net&_nc_cat=101&oh=b9df5fda1b76d6a2b8c64ddc95c15347&oe=5EB0D662"
        )
    )

    val listData: ArrayList<ImageModel>
        get() {
            val list = arrayListOf<ImageModel>()
            for (aData in data) {
                val hero = ImageModel()
                hero.photo = aData[0]
                list.add(hero)
            }
            return list
        }
}