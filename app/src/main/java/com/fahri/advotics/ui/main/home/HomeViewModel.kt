package com.fahri.advotics.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fahri.advotics.ScreenState
import com.fahri.advotics.models.ImageModel

class HomeViewModel(private val findItemsInteractor: FindItemsInteractor) : ViewModel() {

    private lateinit var _mainState: MutableLiveData<ScreenState<HomeState>>

    val mainState: LiveData<ScreenState<HomeState>>
        get() {
            if (!::_mainState.isInitialized) {
                _mainState = MutableLiveData()
                _mainState.value = ScreenState.Loading
                findItemsInteractor.findItems(::onItemsLoaded)
            }
            return _mainState
        }

    private fun onItemsLoaded(items: ArrayList<ImageModel>) {
        _mainState.value = ScreenState.Render(HomeState.ShowItems(items))
    }
}

class HomeViewModelFactory(private val findItemsInteractor: FindItemsInteractor) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(findItemsInteractor) as T
    }
}