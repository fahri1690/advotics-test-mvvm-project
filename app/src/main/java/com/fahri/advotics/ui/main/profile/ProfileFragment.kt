package com.fahri.advotics.ui.main.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.fahri.advotics.R

class ProfileFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileViewModel =
            ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        val textViewName: TextView = root.findViewById(R.id.textViewName)
        profileViewModel.textName.observe(this, Observer {
            textViewName.text = it
        })
        val textViewSex: TextView = root.findViewById(R.id.textViewSex)
        profileViewModel.textSex.observe(this, Observer {
            textViewSex.text = it
        })
        val textViewPhone: TextView = root.findViewById(R.id.textViewPhone)
        profileViewModel.textPhone.observe(this, Observer {
            textViewPhone.text = it
        })
        val imageView: ImageView = root.findViewById(R.id.img_profile)
        profileViewModel.image.observe(this, Observer { image ->
            context?.let {it->
                Glide.with(it)
                    .load(image)
                    .into(imageView)
            }
        })

        return root
    }
}