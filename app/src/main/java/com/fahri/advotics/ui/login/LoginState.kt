package com.fahri.advotics.ui.login

enum class LoginState {
    Success, WrongUserName, WrongPassword
}