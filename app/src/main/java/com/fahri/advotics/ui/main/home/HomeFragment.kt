package com.fahri.advotics.ui.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.fahri.advotics.R
import com.fahri.advotics.ScreenState
import com.fahri.advotics.models.ImageModel
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(
                this,
                HomeViewModelFactory(FindItemsInteractor())
            )[HomeViewModel::class.java]
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        homeViewModel.mainState.observe(this::getLifecycle, this::updateUI)
        return root
    }

    private fun updateUI(screenState: ScreenState<HomeState>?) {
        when (screenState) {
            ScreenState.Loading -> showProgress()
            is ScreenState.Render -> processRenderState(screenState.renderState)
        }
    }

    private fun processRenderState(renderState: HomeState) {
        hideProgress()
        when (renderState) {
            is HomeState.ShowItems -> setItems(renderState.items)
        }
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
        list.visibility = View.GONE
    }

    private fun hideProgress() {
        progress.visibility = View.GONE
        list.visibility = View.VISIBLE
    }

    private fun setItems(items: ArrayList<ImageModel>) {
        list.adapter = HomeAdapter(items)
    }

}