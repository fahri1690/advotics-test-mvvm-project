package com.fahri.advotics.ui.main.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfileViewModel : ViewModel() {

    private val getProfile = GetProfile()

    private val _textName = MutableLiveData<String>().apply {
        getProfile.findItems {
            value = it.name
        }
    }
    val textName: LiveData<String> = _textName

    private val _textSex = MutableLiveData<String>().apply {
        getProfile.findItems {
            value = it.sex
        }
    }
    val textSex: LiveData<String> = _textSex

    private val _textPhone = MutableLiveData<String>().apply {
        getProfile.findItems {
            value = it.phone
        }
    }
    val textPhone: LiveData<String> = _textPhone

    private val _image = MutableLiveData<String>().apply {
        getProfile.findItems {
            value = it.photo
        }
    }
    val image: LiveData<String> = _image

}