package com.fahri.advotics.ui.main.home

import com.fahri.advotics.models.ImageModel

sealed class HomeState {
    class ShowItems(val items: ArrayList<ImageModel>) : HomeState()
}