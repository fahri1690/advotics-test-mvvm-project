package com.fahri.advotics.ui.main.profile

import com.fahri.advotics.`object`.UserDataAccount
import com.fahri.advotics.models.UserAccount

class GetProfile {

    fun findItems(callback: (UserAccount) -> Unit) {
        callback(getAccountData())
    }

    private fun getAccountData(): UserAccount = UserDataAccount.getUser
}