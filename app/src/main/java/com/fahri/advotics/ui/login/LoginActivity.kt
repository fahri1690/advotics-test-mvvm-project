package com.fahri.advotics.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.fahri.advotics.R
import com.fahri.advotics.ScreenState
import com.fahri.advotics.ui.main.HomePage
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel

    private var privateMode = 0
    private val prefName = "user"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val sharedPref = getSharedPreferences(prefName, privateMode)

        if (sharedPref.getBoolean(prefName, false)) {
            val intent = Intent(this, HomePage::class.java)
            startActivityForResult(intent, 200)
        } else {
            viewModel = ViewModelProviders.of(
                this,
                LoginViewModelFactory(LoginInteractor())
            )[LoginViewModel::class.java]

            viewModel.loginState.observe(::getLifecycle, ::updateUI)

            button.setOnClickListener {
                val editor = sharedPref.edit()
                editor.putBoolean(prefName, true)
                editor.apply()
                onLoginClicked()
            }
        }

    }

    private fun updateUI(screenState: ScreenState<LoginState>?) {
        when (screenState) {
            ScreenState.Loading -> progress.visibility = View.VISIBLE
            is ScreenState.Render -> processLoginState(screenState.renderState)
        }
    }

    private fun processLoginState(renderState: LoginState) {
        progress.visibility = View.GONE
        when (renderState) {
            LoginState.Success -> startActivity(Intent(this, HomePage::class.java))
            LoginState.WrongUserName -> username.error = getString(R.string.username_error)
            LoginState.WrongPassword -> password.error = getString(R.string.password_error)
        }
    }

    private fun onLoginClicked() {
        viewModel.onLoginClicked(username.text.toString(), password.text.toString())
    }

}
