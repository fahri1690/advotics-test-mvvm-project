package com.fahri.advotics.ui.main.home

import com.fahri.advotics.`object`.ListImages
import com.fahri.advotics.models.ImageModel
import com.fahri.advotics.postDelayed

class FindItemsInteractor {

    fun findItems(callback: (ArrayList<ImageModel>) -> Unit) {
        postDelayed(500) { callback(createArrayList()) }
    }

    private fun createArrayList(): ArrayList<ImageModel> = ListImages.listData
}