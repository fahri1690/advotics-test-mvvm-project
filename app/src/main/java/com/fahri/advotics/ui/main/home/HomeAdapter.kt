package com.fahri.advotics.ui.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fahri.advotics.R
import com.fahri.advotics.models.ImageModel

class HomeAdapter (private val items: ArrayList<ImageModel>) :
RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_main_item, parent, false) as ImageView

        return HomeViewHolder(v)
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val (image) = items[position]
        Glide.with(holder.itemView.context)
            .load(image)
            .into(holder.imageView)
    }

    override fun getItemCount(): Int = items.size

    class HomeViewHolder(val imageView: ImageView) : RecyclerView.ViewHolder(imageView)
}